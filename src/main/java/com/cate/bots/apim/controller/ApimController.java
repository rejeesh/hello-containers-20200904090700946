package com.cate.bots.apim.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApimController {
	
	@GetMapping(value="/test")
	public String testMethod() {
		
		return "OK-TESTED";
	}

}
